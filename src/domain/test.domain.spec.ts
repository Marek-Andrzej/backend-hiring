import { TestBuilder } from "../shared/builder";
import { TestSupporter } from "../shared/util";
import { TestDomain } from "./test.domain";

// Improve: seperate db to tests
describe("Test controller", () => {
  const testSupporter = new TestSupporter();
  let testDomain: TestDomain;

  beforeEach(() => {
    testSupporter.initializeTestDb();
    testDomain = new TestDomain();
  });

  it("Should create test and save it to db and return", async () => {
    const test = new TestBuilder().defaultValues().build();
    const createdTest = await testDomain.createTest(test);

    expect(createdTest.country).toBe(test.country);
    expect(createdTest.patientName).toBe(test.patientName);
  });

  it("Should throw error when have invalid data", async () => {
    const test = new TestBuilder()
      .defaultValues()
      // @ts-ignore
      .withCustomOverwrite({ patientDateOfBirth: "22asdasdas22" })
      .build();
    try {
      await testDomain.createTest(test);
      expect(false).toBeTruthy();
    } catch (error) {
      expect(error.message).toBe(
        `ValidationError: patientDateOfBirth: Cast to Number failed for value "22asdasdas22" at path "patientDateOfBirth"`
      );
    }
  });

  it("Should collect all tests", async () => {
    const collectedTests = await testDomain.collectTests({
      from: undefined,
      to: new Date(),
    });
    expect(collectedTests.length).not.toBe(0);
  });

  it("Should collect today tests", async () => {
    const collectedTodayTests = await testDomain.collectTodayTests();
    expect(collectedTodayTests.length).not.toBe(0);
  });
});
