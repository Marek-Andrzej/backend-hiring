import { TimeMachine } from "../shared/service";
import { ITest, TestModel } from "../model";
import { Test } from "../shared/type";

export class TestDomain {
  async createTest(test: Test): Promise<ITest> {
    try {
      return await TestModel.create(test);
    } catch (error) {
      throw new Error(error);
    }
  }

  async collectTests(params: { from: Date; to: Date }): Promise<ITest[]> {
    try {
      return await TestModel.find({
        createdAt: {
          $gte: TimeMachine.startOfDay(params.from),
          $lt: TimeMachine.endOfDay(params.to),
        },
      });
    } catch (error) {
      throw new Error(error);
    }
  }
  async collectTodayTests(): Promise<ITest[]> {
    try {
      return await TestModel.find({
        createdAt: {
          $gte: TimeMachine.startOfDay(),
          $lt: TimeMachine.endOfDay(),
        },
      });
    } catch (error) {
      throw new Error(error);
    }
  }
}
