import { HelloController } from "./controller/hello.controller";
import express from "express";
import { Route } from "./shared/enum";
import { TestController } from "./controller";

export function createApp() {
  const helloController = new HelloController();
  const testController = new TestController();

  const app = express();
  app.use(express.json());

  app.use("/", helloController.getRouter());
  app.use(Route.TEST, testController.getRouter());
  return app;
}
