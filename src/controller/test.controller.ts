import { Request, Response, Router } from "express";
import { ITest } from "../model";
import { TestDomain } from "../domain";
import { TestOutcome } from "../shared/enum";
import { calculateTestsPerCountry } from "../shared/util";
import { DateQueryParams, TestsRaport } from "../shared/type";
import { TimeMachine } from "../shared/service";
import { convertTestToTestResponseDto } from "../shared/mappers";
import { validationCheckerMiddleware } from "../middleware";
import { createTestValidationSchema } from "../shared/validation";
import { prepareDateQueryParams } from "../shared/helper";

export class TestController {
  private testDomain = new TestDomain();

  getRouter(): Router {
    const router = Router();
    router.post(
      "/",
      createTestValidationSchema,
      validationCheckerMiddleware,
      this.createTest.bind(this)
    );
    router.get("/", this.generateRaport.bind(this));
    return router;
  }

  async createTest(request: Request, response: Response) {
    try {
      const createdTest = await this.testDomain.createTest(request.body);
      const convertedResponseDto = convertTestToTestResponseDto(createdTest);
      return response.json(convertedResponseDto);
    } catch (error) {
      throw new Error("Failed during creating test");
    }
  }

  async generateRaport(
    request: Request<{}, unknown, unknown, DateQueryParams>,
    response: Response<TestsRaport>
  ) {
    try {
      const dateQueryParams = prepareDateQueryParams(request.query);
      const collectedTests = await this.testDomain.collectTests(
        dateQueryParams
      );
      const raport = this.prepareRaportResponse(
        collectedTests,
        dateQueryParams
      );

      return response.status(200).json(raport);
    } catch (error) {
      throw new Error("Failed during creating test");
    }
  }

  private prepareRaportResponse(
    tests: ITest[],
    dateQueryParams: { from: Date; to: Date }
  ): TestsRaport {
    return {
      raportFrom: dateQueryParams.from
        ? TimeMachine.formatToShortDate(dateQueryParams.from)
        : "∞",
      raportTo: TimeMachine.formatToShortDate(dateQueryParams.to),
      tests: tests.length,
      positiveTests: tests.filter(
        (test) => test.outcome !== TestOutcome.NEGATIVE
      ).length,
      perCountry: calculateTestsPerCountry(tests),
    };
  }
}
