import { RestTestDtoBuilder } from "../shared/builder";
import { Route } from "../shared/enum";
import request from "supertest";
import { createApp } from "../app";
import { TestSupporter } from "../shared/util";
import { TestResponseDto } from "../shared/dto";

// Improve: seperate db to tests
describe("Test controller", () => {
  const testSupporter = new TestSupporter();
  beforeEach(() => {
    testSupporter.initializeTestDb();
  });

  it("Should return raport", async (done) => {
    const response = await request(createApp()).get(Route.TEST);

    expect(response.status).toBe(200);

    expect(response.body).toEqual({
      raportFrom: response.body.raportFrom,
      raportTo: response.body.raportTo,
      tests: response.body.tests,
      positiveTests: response.body.positiveTests,
      perCountry: response.body.perCountry,
    });

    done();
  });

  it("Should create new test in db with response type", async (done) => {
    const restTestDto = new RestTestDtoBuilder().defaultValues().build();

    const response = await request(createApp())
      .post(Route.TEST)
      .send(restTestDto);

    expect(response.status).toBe(200);
    expect(response.body).toEqual<TestResponseDto>({
      country: response.body.country,
      createdAt: response.body.createdAt,
      id: response.body.id,
      lat: response.body.lat,
      long: response.body.long,
      outcome: response.body.outcome,
      patientDateOfBirth: response.body.patientDateOfBirth,
      patientName: response.body.patientName,
    });

    done();
  });

  it("Should throw validation error if rest dto is invalid ", async (done) => {
    const restTestDto = new RestTestDtoBuilder()
      .defaultValues()
      .withInvalidOutcome()
      .build();

    const response = await request(createApp())
      .post(Route.TEST)
      .send(restTestDto);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({
      errors: [
        {
          location: "body",
          msg: "Invalid value",
          param: "outcome",
          value: "INVALID",
        },
      ],
    });

    done();
  });
});
