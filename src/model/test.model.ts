import mongoose from "mongoose";
import { TestOutcome } from "../shared/enum";

export interface ITest extends mongoose.Document {
  patientName: string;
  patientDateOfBirth: number;
  outcome: TestOutcome;
  lat: number;
  long: number;
  country: string;
  createdAt: Date;
  updatedAt: Date;
}

// Improve: Create 2 models patients and tests
export const TestSchema = new mongoose.Schema<ITest>(
  {
    patientName: String,
    patientDateOfBirth: Number,
    outcome: String,
    lat: Number,
    long: Number,
    country: String,
  },
  { timestamps: { createdAt: "createdAt" } }
);

const TestModel = mongoose.model<ITest>("Test", TestSchema);

export default TestModel;
