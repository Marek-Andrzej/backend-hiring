import {
  startOfDay as startOfDayFns,
  endOfDay as endOfDayFns,
  format,
  isValid,
  isAfter as isAfterFns,
} from "date-fns";

export class TimeMachine {
  private static SHORT_DATE_FORMAT = "dd-MM-yyyy";

  static startOfDay(date = new Date()): Date {
    return startOfDayFns(date);
  }
  static endOfDay(date = new Date()): Date {
    return endOfDayFns(date);
  }

  static formatToShortDate(date: Date): string {
    return format(date, this.SHORT_DATE_FORMAT);
  }

  static isValidDate(date: string | Date): boolean {
    return isValid(new Date(date));
  }

  static isAfter(date: Date, dateToCompare: Date): boolean {
    return isAfterFns(date, dateToCompare);
  }
}
