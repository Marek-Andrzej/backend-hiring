import { TestOutcome } from "../enum";

export interface RestTestDto {
  patientName: string;
  patientDateOfBirth: number;
  outcome: TestOutcome;
  lat: number;
  long: number;
  country: string;
}
