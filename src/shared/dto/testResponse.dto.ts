import { TestOutcome } from "../enum";

export interface TestResponseDto {
  id: string;
  patientName: string;
  patientDateOfBirth: number;
  outcome: TestOutcome;
  lat: number;
  long: number;
  country: string;
  createdAt: Date;
}
