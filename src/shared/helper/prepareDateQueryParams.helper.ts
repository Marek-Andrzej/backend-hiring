import { TimeMachine } from "../service";
import { DateQueryParams } from "../type";

export const prepareDateQueryParams = ({ from, to }: DateQueryParams) => {
  const fromDate =
    from && TimeMachine.isValidDate(from) ? new Date(from) : undefined;

  const toDate = to && TimeMachine.isValidDate(to) ? new Date(to) : new Date();

  if (TimeMachine.isAfter(fromDate, toDate)) {
    return { from: new Date(), to: new Date() };
  }
  return { from: fromDate, to: toDate };
};
