import { ITest } from "src/model";
import { TestResponseDto } from "../dto/testResponse.dto";

export const convertTestToTestResponseDto = (test: ITest): TestResponseDto => ({
  id: test._id,
  patientName: test.patientName,
  patientDateOfBirth: test.patientDateOfBirth,
  outcome: test.outcome,
  country: test.country,
  lat: test.lat,
  long: test.long,
  createdAt: test.createdAt,
});
