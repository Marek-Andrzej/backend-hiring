import { checkSchema } from "express-validator";
import { TestOutcome } from "../enum";

export const createTestValidationSchema = checkSchema({
  patientName: { isString: {}, isLength: { options: { min: 3, max: 99 } } },
  patientDateOfBirth: {
    isNumeric: {},
    isFloat: { options: { min: 1900, max: new Date().getFullYear() } },
  },
  outcome: { custom: { options: (value) => value in TestOutcome } },
  lat: {
    isNumeric: {},
    isLength: { options: { min: 2, max: 8 } },
    isFloat: { options: { min: -90, max: 90 } },
  },
  long: {
    isNumeric: {},
    isLength: { options: { min: 2, max: 8 } },
    isFloat: { options: { min: -90, max: 90 } },
  },
  country: { isString: {}, isLength: { options: { min: 2, max: 50 } } },
});
