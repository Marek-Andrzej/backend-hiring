import { connect } from "mongoose";

/**
 * Only for testing purposes
 */
export class TestSupporter {
  async initializeTestDb(): Promise<void> {
    connect("mongodb://treeline:treeline@localhost:27017/jshiring", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }
}
