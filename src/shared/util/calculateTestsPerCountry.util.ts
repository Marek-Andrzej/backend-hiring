import { Test, TestsPerCountry } from "../type";

export const calculateTestsPerCountry = (tests: Test[]): TestsPerCountry => {
  return tests.reduce<TestsPerCountry>((prev, next) => {
    const incrementedValue = prev[next.country] ? ++prev[next.country] : 1;
    return (prev = { ...prev, [next.country]: incrementedValue });
  }, {});
};
