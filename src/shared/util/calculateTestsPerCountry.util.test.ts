import { TestBuilder } from "../builder";
import { calculateTestsPerCountry } from "./calculateTestsPerCountry.util";

describe("calculateTestsPerCountry.util", () => {
  it("Should return corrent numbers of tests per country", () => {
    const testOne = new TestBuilder().defaultValues().build();
    const testTwo = new TestBuilder()
      .defaultValues()
      .withCustomOverwrite({ country: "PL" })
      .build();

    const testsPerCountry = calculateTestsPerCountry([testOne, testTwo]);
    expect(testsPerCountry).toEqual({ US: 1, PL: 1 });
  });
});
