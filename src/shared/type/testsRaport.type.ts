import { TestsPerCountry } from "./testsPerCountry.type";

export interface TestsRaport {
  raportFrom: string;
  raportTo: string;
  tests: number;
  positiveTests: number;
  perCountry: TestsPerCountry;
}
