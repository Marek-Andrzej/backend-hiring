export interface TestsPerCountry {
  [country: string]: number;
}
