export interface DateQueryParams {
  from?: string;
  to?: string;
}
