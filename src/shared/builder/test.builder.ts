import { TestOutcome } from "../enum";
import { Test } from "../type";

export class TestBuilder {
  private test!: Test;

  defaultValues(): TestBuilder {
    this.test = {
      patientName: "test_patient",
      country: "US",
      lat: 22.22,
      long: 23.32,
      outcome: TestOutcome.NEGATIVE,
      patientDateOfBirth: 1990,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    return this;
  }

  withId(id: string): TestBuilder {
    this.test._id = id;
    return this;
  }

  withCustomOverwrite(test: Partial<Test>): TestBuilder {
    this.test = { ...this.test, ...test };
    return this;
  }

  build(): Test {
    return this.test;
  }
}
