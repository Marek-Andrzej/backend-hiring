import { RestTestDto } from "../dto";
import { TestOutcome } from "../enum";

export class RestTestDtoBuilder {
  private restTestDto!: RestTestDto;

  defaultValues(): RestTestDtoBuilder {
    this.restTestDto = {
      patientName: "test_patient",
      country: "US",
      lat: 22.22,
      long: 23.32,
      outcome: TestOutcome.NEGATIVE,
      patientDateOfBirth: 1990,
    };

    return this;
  }

  withInvalidOutcome(): RestTestDtoBuilder {
    //@ts-ignore
    this.restTestDto.outcome = "INVALID";
    return this;
  }

  build(): RestTestDto {
    return this.restTestDto;
  }
}
